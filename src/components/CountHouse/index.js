import React from 'react'
import styles from './CountHouse.module.scss'
import CircleButtonWithContent from '../CircleButtonWithContent'
import ArrowIcon from '../icons/ArrowIcon'
import DotIcon from '../icons/DotIcon'
import PaginationBtn from '../PaginationBtn/index'
import { useState } from 'react'
import useScreenWidth from '../../hooks/useScreenWidth'
const reviewsPage = 4 // переделать	

const CountHouse = ({ count, pagination = false, setMainValuePag }) => {
	const [valuePag, setValuePag] = useState(1)
	const incPag = () => {
		if (valuePag < reviewsPage) {
			const newValue = valuePag + 1
			setValuePag(newValue)
			setMainValuePag(newValue)
		}
	}
	const decPag = () => {
		if (valuePag > 1) {
			const newValue = valuePag - 1
			setValuePag(newValue)
			setMainValuePag(newValue)
		}
	}
	return (
		<div
			className={styles.root}
			style={{
				marginBottom: !pagination ? '1.88rem' : '0',
				
			}}
		>
			<div className={styles.mainContent}>
				<div className={styles.text}>
					<span>{count}</span>
					<span>Доступних для життя будинків</span>
				</div>
				<div className={styles.icons}>
					{!pagination && (
						<CircleButtonWithContent
							propClass='whiteBg'
							linkUrl='/'
							hoverColorBg='var(--dark-primary-color)'
						>
							<DotIcon hoverColor='white' />
						</CircleButtonWithContent>
					)}
					<CircleButtonWithContent
						propClass='whiteBg'
						linkUrl='/'
						hoverColorBg='var(--dark-primary-color)'
						hoverRotate='true'
					>
						<ArrowIcon rotate='90' hoverColor='white' />
					</CircleButtonWithContent>
				</div>
			</div>

			{pagination && (
				<div className={styles.pagination}>
					<PaginationBtn incFun={() => incPag()} decFun={() => decPag()} />
					<CircleButtonWithContent
						propClass='whiteBg'
						linkUrl='/'
						hoverColorBg='var(--dark-primary-color)'
					>
						<span className={styles.selectedPage}>{valuePag}</span>
					</CircleButtonWithContent>
				</div>
			)}
		</div>
	)
}

export default CountHouse

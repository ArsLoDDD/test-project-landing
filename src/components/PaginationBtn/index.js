import React from 'react'
import styles from './PaginationBtn.module.scss'
import ChevronLeft from '../icons/ChevronLeft'
import ChevronRight from '../icons/ChevronRight'

const PaginationBtn = ({ incFun, decFun }) => {
	return (
		<div className={styles.root}>
			<div onClick={() => decFun()}>
				<ChevronLeft />
			</div>
			<div onClick={() => incFun()}>
				<ChevronRight />
			</div>
		</div>
	)
}

export default PaginationBtn

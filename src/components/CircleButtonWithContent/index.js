import React from 'react'
import styles from './CircleButtonWithContent.module.scss'
import classNames from 'classnames'

const CircleButtonWithContent = ({
	propClass,
	children,
	linkUrl,
	propFunc = () => {},
	hoverColorBg,
	hoverRotate,
}) => {
	return (
		<>
			{linkUrl ? (
				<a
					className={classNames(
						styles.root,
						propClass,
						'iconBg',
						hoverRotate && 'arrowHover'
					)}
					href={linkUrl}
					style={{ '--hover-colorBg': hoverColorBg }}
				>
					{children}
				</a>
			) : (
				<div
					className={classNames(
						styles.root,
						propClass,
						'iconBg',
						hoverRotate && 'arrowHover'
					)}
					onClick={() => propFunc()}
					style={{ '--hover-colorBg': hoverColorBg }}
				>
					{children}
				</div>
			)}
		</>
	)
}

export default CircleButtonWithContent

import React from 'react'
import CircleButtonWithContent from '../CircleButtonWithContent'
import HomeIcon from '../icons/HomeIcon'
import PersonIcon from '../icons/PersonIcon'
import styles from './Header.module.scss'
import useScreenWidth from '../../hooks/useScreenWidth'
import MainButton from '../MainButton'

const Header = () => {
	const width = useScreenWidth()
	return (
		<div className={styles.root}>
			<div className={styles.mainRoot}>
				<CircleButtonWithContent
					propClass='secondaryBg'
					hoverColorBg='var(--content-bg)'
					linkUrl='/'
				>
					<HomeIcon color='white' hoverColor='var(--secondary-color)' />
				</CircleButtonWithContent>
				{(width === 'tablet' || width === 'desktop') && (
					<div className={styles.headerBtnBox}>
						<MainButton
							text='Головна'
							propClass='whitePrimaryBtn'
							linkUrl='/'
						/>
						<MainButton
							text='Про нас'
							propClass='whitePrimaryBtn'
							linkUrl='/'
						/>
						<MainButton text='Оренда' propClass='whitePrimaryBtn' linkUrl='/' />
						<MainButton
							text='Контакти'
							propClass='whitePrimaryBtn'
							linkUrl='/'
						/>
					</div>
				)}
				<CircleButtonWithContent
					propClass='contentBg'
					hoverColorBg='var(--secondary-color)'
				>
					<PersonIcon hoverColor='var(--content-bg)' />
				</CircleButtonWithContent>
			</div>
		</div>
	)
}

export default Header

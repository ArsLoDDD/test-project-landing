import React from 'react'
import styles from './TextBlock.module.scss'
import CircleButtonWithContent from '../CircleButtonWithContent'
import ArrowIcon from '../icons/ArrowIcon'
import useScreenWidth from '../../hooks/useScreenWidth'
import Quoticon from '../icons/Quoticon'

const TextBlock = ({
	title,
	content,
	img,
	maxH,
	width,
	fontSize,
	fullWidth,
	notArrow = false,
}) => {
	const userWidth = useScreenWidth()
	return (
		<div
			className={styles.root}
			style={{
				width: `${width ? `${width}%` : '85%'}`,
				height: `${maxH ? `${maxH}rem` : 'auto'}`,
			}}
		>
			{title ? (
				<>
					<div className={styles.titleBox}>
						{img ? (
							<div className={styles.withAvatar}>
								<img className={styles.avatar} src={img} alt={title} />
								<div className={styles.nameBox}>
									<h5>{title}</h5>
									{userWidth !== 'mobile' && <span>Житель Dream</span>}
								</div>
							</div>
						) : (
							<h3
								style={{
									fontSize: userWidth === 'desktop' ? '1.1rem' : '1.25rem',
								}}
							>
								{title}
							</h3>
						)}

						{!notArrow && (
							<CircleButtonWithContent
								propClass='primaryBg'
								linkUrl='/'
								hoverColorBg='var(--dark-primary-color)'
								hoverRotate='true'
							>
								<ArrowIcon rotate='90' hoverColor='white' color='white' />
							</CircleButtonWithContent>
						)}
						{notArrow && userWidth === 'desktop' && (
							<CircleButtonWithContent>
								<Quoticon width='48' height='48' />
							</CircleButtonWithContent>
						)}
					</div>
					<p
						style={{
							fontSize: `${img ? '1.125' : '1.375'}rem`,
							fontSize: `${fontSize ? `${fontSize}rem` : '1.375rem'}`,
							width: `${img ? '100%' : fullWidth ? '100%' : '85%'}`,
						}}
					>
						{content}
					</p>
				</>
			) : (
				<div className={styles.withOutTitle}>
					<p
						style={{
							fontSize: '1.375rem',
						}}
					>
						{content}
					</p>
					<CircleButtonWithContent
						propClass='primaryBg'
						linkUrl='/'
						hoverColorBg='var(--dark-primary-color)'
						hoverRotate='true'
					>
						<ArrowIcon rotate='90' hoverColor='white' color='white' />
					</CircleButtonWithContent>
				</div>
			)}
		</div>
	)
}

export default TextBlock

import React from 'react'
import CircleButtonWithContent from '../CircleButtonWithContent'
import MainButton from '../MainButton'
import ArrowIcon from '../icons/ArrowIcon'
import styles from './ItemBox.module.scss'

const ItemBox = ({
	id,
	description,
	itemName,
	imgLink,
	height,
	textBtn,
}) => {
	return (
		<div
			className={styles.root}
		>
			<img
				style={{
					height: height ? `${height}rem` : '',
				}}
				src={imgLink}
				alt=''
			/>
			<div className={styles.textBox}>
				<h3>{itemName}</h3>
				<p>{description}</p>
			</div>
			<div className={styles.iconBox}>
				<MainButton text={textBtn ? textBtn : 'Більше'} propClass='whiteBtn' />
				<CircleButtonWithContent
					propClass='primaryBg'
					linkUrl='/'
					hoverColorBg='var(--dark-primary-color)'
					hoverRotate='true'
				>
					<ArrowIcon rotate='90' hoverColor='white' color='white' />
				</CircleButtonWithContent>
			</div>
		</div>
	)
}

export default ItemBox

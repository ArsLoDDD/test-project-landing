import React from 'react'
import { useField } from 'formik'
import styles from './Input.module.scss'
import { PatternFormat } from 'react-number-format'
import useScreenWidth from '../../../hooks/useScreenWidth'
import { useState } from 'react'

const Input = props => {
	const [field, meta] = useField(props)
	const { error, touched } = meta
	const width = useScreenWidth()
	const [isHovered, setIsHovered] = useState(false)

	const handleChange = e => {
		field.onChange(e)
		props.onChange && props.onChange(e)
	}
	return (
		<div className={styles.root}>
			{props.phone ? (
				<PatternFormat
					// попытаться пофиксить валидацию телефона
					format='+38 (###) ###-##-##'
					mask='_'
					className={styles.input}
					id={field.name}
					{...field}
					{...props}
					onValueChange={values => {
						handleChange({ target: { name: field.name, value: values.value } })
					}}
				/>
			) : (
				<input
					id={field.name}
					className={styles.input}
					{...field}
					{...props}
					onChange={handleChange}
				/>
			)}
			{width !== 'desktop' && error && touched && (
				<div className={styles.error}>{error}</div>
			)}
			{width === 'desktop' && error && touched && (
				<div
					className={styles.errorIcon}
					onMouseEnter={() => setIsHovered(true)}
					onMouseLeave={() => setIsHovered(false)}
				>
					<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'>
						<path d='M13,13H11V7H13M13,17H11V15H13M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z' />
					</svg>
					{isHovered && (
						<div className={styles.errorText}>
							<p>{error}</p>
						</div>
					)}
				</div>
			)}
		</div>
	)
}

export default Input

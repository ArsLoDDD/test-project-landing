import * as Yup from 'yup'

const validationSchema = Yup.object().shape({
	name: Yup.string()
		.required('Ім’я повинно бути заповнене')
		.min(3, 'Мінімум 3 символи'),
	lastName: Yup.string()
		.required('Прізвище повинно бути заповнене')
		.min(3, 'Мінімум 3 символи'),
	phone: Yup.string().required('Телефон повинен бути заповнений'),
	kidsCount: Yup.number()
		.required('Кількість дітей повинна бути заповнене')
		.typeError('Кількість дітей повинна бути числовим значенням')
		.min(0, 'Мінімум 0 дітей')
		.max(10, 'Максимум 10 дітей'),
	disability: Yup.string().max(30, 'Максимум 30 символів'),
	income: Yup.number()
		.required('Доход повинен бути заповнен')
		.typeError('Доход повинен бути числовим значенням')
		.min(0, 'Мінімум дохід 0')
		.max(1000000, 'Максимум доход 1 000 000'),
	// .min(10, 'Введіть повний номер телефону')
	// .test('valid-phone', 'Введіть повний номер телефону', value => {
	// 	const cleanValue = value.replace(/\D/g, '')
	// 	return cleanValue.length === 10
	// }),
})

export default validationSchema

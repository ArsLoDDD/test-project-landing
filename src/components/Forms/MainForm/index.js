import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import Input from '../Input'
import validationSchema from './validationSchema'
import styles from './MainForm.module.scss'
import CircleButtonWithContent from '../../CircleButtonWithContent'
import DotIcon from '../../icons/DotIcon'
import axios from 'axios'
import AboutUsInfo from '../../Main/AboutUs/AboutUsInfo'
import useScreenWidth from '../../../hooks/useScreenWidth'
import ArrowIcon from '../../icons/ArrowIcon'
import CountHouse from '../../CountHouse'

const MainForm = ({ setMainValuePag }) => {
	const initialValuesMobile = {
		name: '',
		lastName: '',
		phone: '',
	}

	const initialValuesDesktop = {
		name: '',
		lastName: '',
		phone: '',
		kidsCount: '',
		disability: '',
		income: '',
	}
	const [inputsChanged, setInputsChanged] = useState(false)
	const width = useScreenWidth()

	const handleSubmit = async (values, action) => {
		console.log(values)

		// try {
		// 	const { data } = await axios.post('https://test.test.com/orders', values)
		// } catch (error) {
		// 	console.log(error)
		// }
		action.resetForm()
	}

	return (
		<div className={styles.mainRoot}>
			{width === 'desktop' && (
				<CountHouse
					pagination={true}
					count='342'
					setMainValuePag={setMainValuePag}
				/>
			)}
			<div className={styles.root}>
				<div className={styles.formHeading}>
					<h2>Залишіть заявку</h2>
					<div className={styles.iconBox}>
						<CircleButtonWithContent
							propClass='whiteBg'
							linkUrl='/'
							hoverColorBg='var(--dark-primary-color)'
							hoverColor='white'
						>
							<DotIcon hoverColor='white' />
						</CircleButtonWithContent>
						{width === 'desktop' && (
							<CircleButtonWithContent
								propClass='primaryBg'
								linkUrl='/'
								hoverColorBg='var(--dark-primary-color)'
								hoverRotate='true'
							>
								<ArrowIcon rotate='90' hoverColor='white' color='white' />
							</CircleButtonWithContent>
						)}
					</div>
				</div>
				<Formik
					initialValues={
						width === 'desktop' ? initialValuesDesktop : initialValuesMobile
					}
					onSubmit={handleSubmit}
					validationSchema={validationSchema}
					enableReinitialize={true}
				>
					{form => (
						<Form>
							<div className={styles.gridContainer}>
								<Input
									name='name'
									placeholder='Ім’я'
									onChange={() => setInputsChanged(true)}
								/>
								<Input
									name='lastName'
									placeholder='Прізвище'
									onChange={() => setInputsChanged(true)}
								/>
								<Input
									phone='true'
									name='phone'
									placeholder='Номер телефону'
									onChange={() => setInputsChanged(true)}
								/>
								{width === 'desktop' && (
									<>
										<Input
											name='kidsCount'
											placeholder='Кількість дітей'
											onChange={() => setInputsChanged(true)}
										/>
										<Input
											name='disability'
											placeholder='Наявність інвалідності'
											onChange={() => setInputsChanged(true)}
										/>
										<Input
											name='income'
											placeholder='Доходи'
											onChange={() => setInputsChanged(true)}
										/>
									</>
								)}
							</div>

							{width !== 'desktop' && (
								<button
									className={styles.submitBtn}
									type='submit'
									disabled={!form.isValid || !inputsChanged}
								>
									Залишити заявку
								</button>
							)}
						</Form>
					)}
				</Formik>
			</div>
			{width === 'tablet' && <AboutUsInfo width={width} />}
		</div>
	)
}

export default MainForm

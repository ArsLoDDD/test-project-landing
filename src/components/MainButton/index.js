import React from 'react'
import styles from './MainButton.module.scss'
import classNames from 'classnames'

const MainButton = ({ text, bgColor = 'white', propClass, linkUrl }) => {
	return (
		<>
			{linkUrl ? (
				<a href={`${linkUrl}`} className={classNames(styles.root, propClass)}>
					{text}
				</a>
			) : (
				<button className={classNames(styles.root, propClass)}>{text}</button>
			)}
		</>
	)
}

export default MainButton

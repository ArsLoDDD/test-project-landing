import React from 'react'
const ChevronLeft = ({ width, height, color = '#202026', hoverColor }) => (
	<svg
		viewBox='0 0 9 16'
		width={width || '9'}
		height={height || '16'}
		fill={color || 'currentColor'}
		stroke='none'
		className='icon chevron'
		style={{ '--hover-color': hoverColor }}
	>
		{
			<path
				d='M2 7C2.55228 7 3 7.44772 3 8C3 8.55228 2.55228 9 2 9L2 7ZM0.292893 8.70711C-0.0976309 8.31658 -0.097631 7.68342 0.292893 7.29289L6.65685 0.928932C7.04738 0.538407 7.68054 0.538407 8.07107 0.928932C8.46159 1.31946 8.46159 1.95262 8.07107 2.34315L2.41421 8L8.07107 13.6569C8.46159 14.0474 8.46159 14.6805 8.07107 15.0711C7.68054 15.4616 7.04738 15.4616 6.65685 15.0711L0.292893 8.70711ZM2 9L1 9L1 7L2 7L2 9Z'
				fill='#202026'
			/>
		}
	</svg>
)

export default ChevronLeft

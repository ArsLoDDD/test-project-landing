const ArrowIcon = ({
	width,
	height,
	color = '#202026',
	rotate,
	hoverColor,
}) => {
	return (
		<svg
			viewBox='0 0 24 24'
			width={width || '24'}
			height={height || '24'}
			fill={color}
			stroke='none'
			preserveAspectRatio='none'
			style={{ transform: `rotate(${rotate}deg)`, '--hover-color': hoverColor }}
			className='icon'
		>
			<path
				d='M19,17.59L17.59,19L7,8.41V15H5V5H15V7H8.41L19,17.59Z'
			/>
		</svg>
	)
}

export default ArrowIcon

import React from 'react'
const HomeIcon = ({ width, height, color, hoverColor }) => (
	<svg
		viewBox='0 0 24 24'
		width={width || '24'}
		height={height || '24'}
		fill={color || 'currentColor'}
		stroke='none'
		className='icon'
		style={{ '--hover-color': hoverColor }}
	>
		{
			<path d='M12 5.69L17 10.19V18H15V12H9V18H7V10.19L12 5.69M12 3L2 12H5V20H11V14H13V20H19V12H22' />
		}
	</svg>
)

export default HomeIcon

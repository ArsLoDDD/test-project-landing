import React from 'react'
const Quoticon = ({ width, height, color = '#534d96', hoverColor }) => (
	<svg
		viewBox='0 0 24 24'
		width={width || '24'}
		height={height || '24'}
		fill={color || 'currentColor'}
		className='icon'
		style={{ '--hover-color': hoverColor }}
	>
		{<path d='M10,7L8,11H11V17H5V11L7,7H10M18,7L16,11H19V17H13V11L15,7H18Z' />}
	</svg>
)

export default Quoticon

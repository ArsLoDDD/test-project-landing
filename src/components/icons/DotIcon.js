import React from 'react'
const DotIcon = ({ width, height, color = '#202026', hoverColor }) => (
	<svg
		viewBox='0 0 24 24'
		width={width || '24'}
		height={height || '24'}
		fill={color || 'currentColor'}
		stroke='none'
		className='icon'
		style={{ '--hover-color': hoverColor }}
	>
		{
			<path d='M16,12A2,2 0 0,1 18,10A2,2 0 0,1 20,12A2,2 0 0,1 18,14A2,2 0 0,1 16,12M10,12A2,2 0 0,1 12,10A2,2 0 0,1 14,12A2,2 0 0,1 12,14A2,2 0 0,1 10,12M4,12A2,2 0 0,1 6,10A2,2 0 0,1 8,12A2,2 0 0,1 6,14A2,2 0 0,1 4,12Z' />
		}
	</svg>
)

export default DotIcon

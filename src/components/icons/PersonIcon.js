import React from 'react'
const PersonIcon = ({ width, height, color = '#202026', hoverColor }) => (
	<svg
		viewBox='0 0 24 24'
		width={width || '24'}
		height={height || '24'}
		fill={color || 'currentColor'}
		className='icon'
		style={{ '--hover-color': hoverColor }}
	>
		{
			<path d='M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z' />
		}
	</svg>
)

export default PersonIcon

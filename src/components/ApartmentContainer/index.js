import React, { useEffect, useState } from 'react'
import CircleButtonWithContent from '../CircleButtonWithContent'
import ItemBox from '../ItemBox'
import PaginationBtn from '../PaginationBtn'
import styles from './ApartmentContainer.module.scss'
import useScreenWidth from '../../hooks/useScreenWidth'
import apartmentArray from './dataArray'
import Spinner from '../Spinner'
import MainButton from '../MainButton'

const ApartmentContainer = ({ value }) => {
	const [valuePag, setValuePag] = useState(1)
	const [itemsArray, setItemsArray] = useState(apartmentArray)
	const [isLoading, setIsLoading] = useState(false)
	const width = useScreenWidth()
	const [showArray, setShowArray] = useState([])

	let itemsToShow = 4

	if (width === 'tablet') {
		itemsToShow = 6
	}
	if (width === 'desktop') {
		itemsToShow = 7
	}
	let pageCount = Math.ceil(itemsArray.length / itemsToShow)
	useEffect(() => {
		if (value !== null) {
			setValuePag(value)
		}
	}, [value])

	useEffect(() => {
		const start = (valuePag - 1) * itemsToShow
		const end = start + itemsToShow
		setIsLoading(true)
		setTimeout(() => {
			setShowArray(itemsArray.slice(start, end))
			setIsLoading(false)
		}, 400)
	}, [valuePag, itemsArray])

	useEffect(() => {
		setIsLoading(true)
		setTimeout(() => {
			setItemsArray(apartmentArray.slice(0, itemsToShow * pageCount))
			setValuePag(1)
			setIsLoading(false)
		}, 400)
	}, [width])

	const incPag = () => {
		if (valuePag < pageCount) {
			setValuePag(prev => prev + 1)
		}
	}

	const decPag = () => {
		if (valuePag > 1) {
			setValuePag(prev => prev - 1)
		}
	}

	return (
		<>
			<div className={styles.root}>
				<>
					{width === 'desktop' && (
						<div className={styles.additionalItem}>
							<article>
								<h3>Каталог квартир</h3>
								<p>
									Ми є компанією з оренди соціального житла, яка прагне надавати
									доступне та якісне житло людям з низьким доходом.
								</p>
							</article>

							<MainButton
								linkUrl='/'
								text='Переглянути каталог'
								propClass='whiteBtn'
							/>
						</div>
					)}
					{showArray.map(({ street, id, description, imgLink }) => (
						<>
							{isLoading ? (
								<div className={styles.spinnerBox}>
									<Spinner />
								</div>
							) : (
								<ItemBox
									itemName={street}
									imgLink={imgLink}
									description={description}
									textBtn={width === 'desktop' ? 'Більше інформації' : null}
								/>
							)}
						</>
					))}
				</>
			</div>
			{width !== 'desktop' && (
				<div className={styles.paginationBox}>
					<div className={styles.paginationPages}>
						{valuePag === 1 && <span className={styles.disableSpan}> 0 </span>}
						{valuePag !== 1 && <span>{valuePag - 1}</span>}
						<CircleButtonWithContent
							propClass='whiteBg'
							hoverColorBg='var(--secondary-color)'
						>
							<span className={styles.selectedPage}>{valuePag}</span>
						</CircleButtonWithContent>
						{valuePag < pageCount && <span>{valuePag + 1}</span>}
						{valuePag + 2 <= pageCount && <span>...</span>}
					</div>

					<PaginationBtn incFun={incPag} decFun={decPag} />
				</div>
			)}
		</>
	)
}

export default ApartmentContainer

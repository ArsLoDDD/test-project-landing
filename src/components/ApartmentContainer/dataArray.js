const urlFrst =
	'https://s3-alpha-sig.figma.com/img/b204/3a03/6fe7f156f46a9bd16d6e7f30fbe8a172?Expires=1693180800&Signature=BWzbo5-NyUrV5rIrx~s1H47wb3ogc3RhR8GktGKjAXu~PcimNjLytAKgNniiZNtlsMoOK2DEKawHEUegA1qOLnx3Y0UgBtCVuSiBZtUETlQAy82s8fGV16nuShusdGsij2jBaLpC6iL8fe4Uc4u-7BkXfQk9XxduTe6KAM3wWjPNq6Dd0fCXejLbaR5PIhqaH~3SrAhWZNQHC-jZsHpEptpwW5QqGIVe~-SxuettM9yaZ183vG7ozX9oQco3h7J8F-Z6QX075SIpD9FcD4wqSejEhHeuswavZEUUaikB~SgnPmauXgPOTjVy1TqFJf2uIpFP0Bmz2FCB2DENndTD7g__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'

const urlScnd =
	'https://s3-alpha-sig.figma.com/img/d3de/acf9/49c6970830c6c7cc449ef20ef5bee6f6?Expires=1693180800&Signature=pvcy36TNwfwz36pLHjlJESEEnKrGOFBlrkAF6dAY3u7YfGYqOGT0MCPcyucTnodt9a36cMqazL8fBfe2rT-okMlet-6d6r46MF7DrdAsIaUAYbHlIiS5hUQBYoXLjFDf1KGthQBNzJz91CNFCLm~LYrFEn1gQRiYcfQ2IZ4WgA54qleJcFxoJNOOuw4q-UA7k2mvbYnzG4o92NIVoN0vUhrbGrUCvEvJ-aDz0G8-OgaDBtLytlBfOOkF10YgiJFOLY7FmdphE4WvzfTZ~9gqL-Swn3EihdK0NeSgherNt~FPOI9a-i4VF~YxlMYuPPeokzAJf4iM5~QDQWuFRNXYmg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'

const urlThrd =
	'https://s3-alpha-sig.figma.com/img/1555/eb5e/4b1b3b0477749b12e9e3da01d4aed36a?Expires=1693180800&Signature=dpfx5PdTgDl1Vox~c9TQdEItxR0uQ4j6U-otd08gIPuiwgr3gvEgHRyspdmppKQIhk9IOcmGkSm86GGuwK-uxXbyIea8ZR52eHvZGNNUFvzwCXwUHnlwlXElif3rsTukonvYQf7WWYMoRfxp8Yb-P4P42X9p8G1-XrH0dVMApzmlOobdAmHfAmaFYJdjDPeAKMtzUYTK0TXM3S1NpM7ApIOrAITZfhKQskJ6aWFyqzG8gBqib8O9KdARgwjfoSHNldVsX~XJctLZAka8KGqoVFXGkqZPm5GsC2pMRYHXJuW~i6WTgveKQ8y2uhKL-S1TTCEErYQHTEg57N09LcQbEA__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'

const urlFrth =
	'https://s3-alpha-sig.figma.com/img/44be/c76d/abfd4fc415c77cf9033c216d22d9045e?Expires=1693180800&Signature=hEtVnONxo3dwsN~p4ygI2ztbcTOVhFcZLU2J2vPZIUOPfeGyVRTmxFDKv6Rl0cLF0FmPZjuH0Ma1QduhbZR7-saASsTOKg9voX33A2SKDKcYmmN~pwNbbgDe181Y5vI41EjPnUZVxQYvBYv7EG8hWskOp~~jDHlux5rSOJkDYeXc3xWbe-kHn5gAvFp3GSvFuF9lhS6ke-rvdrYhjZL2z79aJTlXM~BfYNiWujoUzsWS69cJeKLCnzMRRMC7i5xxGQFinBMKt4VL1HG9fLdWlsnAazJGHn5LKpb-jbidYmSN7JWqibiAYDtSY49A1n~z2TslDj-y4D9~KBlJaQXI1w__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'

const apartmentArray = [
	{
		street: 'Вул. Ювілейна',
		imgLink: urlFrst,
		id: '1',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Перемоги',
		imgLink: urlScnd,
		id: '2',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Луцька',
		imgLink: urlThrd,
		id: '3',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Львівська',
		imgLink: urlFrth,
		id: '4',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Шевченка',
		imgLink: urlFrst,
		id: '5',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Київська',
		imgLink: urlScnd,
		id: '6',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Миру',
		imgLink: urlThrd,
		id: '7',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Хрещатик',
		imgLink: urlFrth,
		id: '8',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},

	{
		street: 'Вул. Бандери',
		imgLink: urlFrst,
		id: '9',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Мечнікова',
		imgLink: urlScnd,
		id: '10',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Старокиївська',
		imgLink: urlThrd,
		id: '11',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Грушевського',
		imgLink: urlFrth,
		id: '12',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Соборна',
		imgLink: urlFrst,
		id: '13',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Франка',
		imgLink: urlScnd,
		id: '14',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Мазепи',
		imgLink: urlThrd,
		id: '15',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Хмельницького',
		imgLink: urlFrth,
		id: '16',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Курортна',
		imgLink: urlFrst,
		id: '17',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Соборна',
		imgLink: urlScnd,
		id: '18',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Київська',
		imgLink: urlThrd,
		id: '19',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
	{
		street: 'Вул. Львівська',
		imgLink: urlFrth,
		id: '20',
		description:
			'Новобудова, ЖК "Америка", євроремонт, кухня- студія loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem',
	},
]

export default apartmentArray

import React from 'react'
import styles from './Main.module.scss'
import CountHouse from '../CountHouse'
import MainForm from '../Forms/MainForm'
import CircleButtonWithContent from '../CircleButtonWithContent'
import ArrowIcon from '../icons/ArrowIcon'
import ApartmentContainer from '../ApartmentContainer'
import Reviews from './Reviews'
import AboutUs from './AboutUs'
import { useState } from 'react'
import Questions from './Questions'
import useScreenWidth from '../../hooks/useScreenWidth'
const Main = () => {
	const width = useScreenWidth()
	const [valuePag, setValuePag] = useState(0)

	const setMainValuePag = value => {
		setValuePag(value)
	}
	return (
		<div className={styles.root}>
			<div className={styles.mainRoot}>
				{width !== 'desktop' && (
					<>
						<h1 className={styles.mainHeading}>
							Вибирай безпечні умови для життя
						</h1>
						<p className={styles.mainText}>
							Ми є компанією з оренди соціального житла, яка прагне надавати
							доступне та якісне житло людям з низьким доходом.
						</p>
					</>
				)}
				{width === 'desktop' && (
					<div className={styles.textBoxWrapper}>
						<div>
							<span>Вибери</span>
						</div>
						<div>
							<p>Якісні</p>
							<p>Безпечні</p>
							<p>Комфортні</p>
						</div>
						<div>
							<span>умови для життя</span>
						</div>
					</div>
				)}
				{width !== 'desktop' && <CountHouse count='342' />}
				<MainForm setMainValuePag={setMainValuePag} />
				{width !== 'desktop' && (
					<div className={styles.catalogy}>
						<span>Каталог квартир</span>
						<CircleButtonWithContent
							propClass='primaryBg'
							linkUrl='/'
							hoverColorBg='var(--dark-primary-color)'
							hoverRotate='true'
						>
							<ArrowIcon rotate='180' hoverColor='white' color='white' />
						</CircleButtonWithContent>
					</div>
				)}
				<ApartmentContainer value={valuePag} />
				<AboutUs />

				{width !== 'desktop' && (
					<>
						<Reviews />
						<Questions />
					</>
				)}
				{width === 'desktop' && (
					<>
						<Questions />
						<Reviews />
					</>
				)}
			</div>
		</div>
	)
}

export default Main

import React from 'react'
import TextBlock from '../../TextBlock'
import styles from './Questions.module.scss'
import dataArray from './dataArray'
import useScreenWidth from '../../../hooks/useScreenWidth'
import { useState } from 'react'
import CircleButtonWithContent from '../../CircleButtonWithContent'
import DotIcon from '../../icons/DotIcon'
import classNames from 'classnames'
import PaginationBtn from '../../PaginationBtn'

const reviewsPage = 5

const Questions = () => {
	const [valuePag, setValuePag] = useState(1)
	const incPag = () => {
		if (valuePag < reviewsPage) {
			setValuePag(valuePag + 1)
		}
	}
	const decPag = () => {
		if (valuePag > 1) {
			setValuePag(valuePag - 1)
		}
	}

	const width = useScreenWidth()
	let numberOfItems = 4
	if (width === 'desktop') {
		numberOfItems = 6
	}
	return (
		<div className={styles.root}>
			{width !== 'desktop' && <h3>Популярні запитання</h3>}
			{width === 'desktop' && (
				<div className={styles.title}>
					<div className={styles.titleBox}>
						<h3>Популярні запитання</h3>
						<div className={styles.paginationBox}>
							<CircleButtonWithContent
								propClass='whiteBg'
								hoverColorBg='var(--secondary-color)'
							>
								<span className={classNames(styles.selectedPage, 'whiteText')}>
									{valuePag}
								</span>
							</CircleButtonWithContent>
							<PaginationBtn incFun={incPag} decFun={decPag} />
						</div>
					</div>
					<div className={styles.circleButton}>
						<CircleButtonWithContent
							propClass='whiteBg'
							hoverColorBg='var(--secondary-color)'
						>
							<DotIcon hoverColor='white' />
						</CircleButtonWithContent>
					</div>
				</div>
			)}
			<div className={styles.itemBox}>
				{dataArray.slice(0, numberOfItems).map(item => (
					<div className={styles.item} key={item.id}>
						<TextBlock
							content={item.question}
							maxH={width === 'desktop' && '4.2'}
						/>
					</div>
				))}
			</div>
		</div>
	)
}

export default Questions

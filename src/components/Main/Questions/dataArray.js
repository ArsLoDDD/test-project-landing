const data = [
	{
		id: 1,
		question: 'Як я можу дізнатися, чи маю я право на соціальне житло?',
	},
	{
		id: 2,
		question: 'Як довго триває процес розгляду заявки на соціальне житло?',
	},
	{
		id: 3,
		question: 'Які права та обов’язкиу мене як у орендаря соціального житла?',
	},
	{
		id: 4,
		question: 'Який розмір орендної плати за соціальне житло?',
	},
	{
		id: 5,
		question:
			'Які документи мені потрібно надати для заявки на соціальне житло?',
	},
	{
		id: 6,
		question: 'Що робити, якщо я порушив умови оренди соціального житла?',
	},
]

export default data
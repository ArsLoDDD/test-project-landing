import React, { useEffect } from 'react'
import styles from './Reviews.module.scss'
import TextBlock from '../../TextBlock'
import CircleButtonWithContent from '../../CircleButtonWithContent'
import PaginationBtn from '../../PaginationBtn'
import classNames from 'classnames'
import useScreenWidth from '../../../hooks/useScreenWidth'
import { useState } from 'react'
import DotIcon from '../../icons/DotIcon'

const reviews = [
	{
		img: 'https://s3-alpha-sig.figma.com/img/317d/e567/bfebba8852c55c6d88733f6b810c30d5?Expires=1693180800&Signature=Bpw64EdSq6Gvxpvg~KEPd4MDH~t7QvAAfgtSo7DeF6DDhRFo3QQh~hmtHeWwVxjMgoK6Z4FYmwqg6ZX4pAPjyhPXCsyA0nR6tpZuKpIdWD5FEcSo-7hZ87dLTDn74wLsCo~M4ybC9pO--vyo5WhqC~UYM6QKW~A~OFNVuLX7A5HDNHt3ILRE2tyExQHNnj1ma~CvyZ8fuiKmp4TMknbEufx84-VXi94KT5QAdn1dNR1bkwlBUD9Hz92iSsFb4owWAoB28ySg7IUK2oLFDmiK7LzSBOLxp011wbwnxL93RSIhvUMpob8R64UeuqVZ01hVVuIBtdz0zrK6m7EgRIHJdQ__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4',
		title: 'Діма Кері',
		userId: '1',
		content:
			'Я дуже вдячний цій компанії за допомогу в пошуку житла. Я був у складній ситуації, і вони допомогли мені знайти.',
	},
	{
		img: 'https://s3-alpha-sig.figma.com/img/8252/dff0/74bbf2ed399496a6b6867770170d23e9?Expires=1693180800&Signature=gJHGR9wPfBXon7dvi7hj-Gj8gJrU6kvshcrSJ~v6qEILuHsXCsdKAX-xbEs~9BfqfgBL2s9XGfB20atoGcsS0m-l-SEapKqsNyDAiJjSlo2fpFUyIA~RW-nfYEUj8-ZPo7y2gbZpwJBG8LqZQkh~uoO7xhj3GrDD1d1WepT85YCzfxsHd1iooSUmFIixLa3MOE1tGnwR~Nq9vCAJHAJNWMdKuxl1ZCjuSeBoLFpPWs7-9O6qdbnMB0l2pPWjb-HjOoAcjtBCKqOT8lx55riShuKDp4582jRrkQN8OKanTev4Sle3kHRnavcQ-oPGmzzdOK~Hpa8UGfYgvIj0zTL4sw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4',
		title: 'Тарас Булька',
		userId: '2',
		content:
			'Я живу в цьому житлі вже кілька років і дуже задоволена. Персонал завжди уважни і готові допомогти. Жило чисте і добре обслуговується.',
	},
	{
		img: 'https://s3-alpha-sig.figma.com/img/c14e/8e03/dfc76543d96979a9fec9885de3da1917?Expires=1693180800&Signature=ormR0Kyo4vsPjEyFXYMyCu5MStEZe4S7am2jOK9cXeUpNLdPfWDOzOWmdCvZ0lbVhby43lwn1N1cVbm0sOjzvTi7g69ok5h7B057cC4jBIDWSMZCt1tKE0Nechgy17F5s1i~Li2R7HzaITCLWfChwZLVaKRqR89ZJX11HPTpBMF20e-TSsN6yWhTm9mo9OUTCVJFSTIuD7EFVqXA8yceGoXhTQJEWiWtLpfLJtiD6K9ZlNckKn7AF1ALFdmcgBMclI01O2S51xhTAS1BuufYqxjDThdvoxOhSXum2IPTb-svEHOCUVIav-XYcKOCHGkvYccPX6irYTPlKzj02poPlw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4',
		title: 'Саша Заведько',
		userId: '3',
		content:
			'Вони завжди готові допомогти, і вони завжди працюють над тим, щоб зробити житло комфортнішим і безпечнішим.',
	},
	{
		img: 'https://s3-alpha-sig.figma.com/img/33c1/715e/7264e5530f5e793fa01f84e138175b71?Expires=1693180800&Signature=VnXxANYFgIc8OOzvJTaCHaBeT4kz-bXpmu1xelSaVdKVOmjfb2pOrn6i~RBPW0gv4A0MtL3zYZnI9n0EFQhFpC7BEdM2hYq-ixZg5MF60UCImTtksF8d53PVF29mUqfb8hfs2U5yrpO2oZmVhF-kJ4d8yJNR9qgcXE4phpQoKXbPfc0nlCD5Dun0xjVkC4zOwDxPr7s74x-HBZULUYWjxhH8Xynl2SdEJSdFglt3aDbHfYDen5dEzjVRnmeUa55-sOrS7cN3-FFS5JcRicSz7t~hwLtq-sLPtw5DSAMHtHgEB4XjrmqLG4vjP7n2EqDG1R5TYbou0RDpYjWtig~3vA__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4',
		title: 'Маша Заєць',
		userId: '4',
		content:
			'Я живу в цьому житлі вже кілька років і дуже задоволена. Персонал завжди уважний і готові допомогти.',
	},
	{
		img: 'https://s3-alpha-sig.figma.com/img/68a4/f735/7b063238d7f15924cccc68ca864f5bbf?Expires=1693180800&Signature=Dqt1dIlsnkTEEfczop1wtmKCovX0HHwc~tTitorm3iIQsK1YAa8HzrExIKNvEGi3wZ~ba9vFz2WSTJuEo7ZPzT9PxV1fbCOPYZ3VxeCcmcq0wdtduusPW6ILyIAxvUBylk~xeAhmHwwDb4FbNdWH4CjAuQuUsKz6G4FbO4E4lsIQsHWaJ3m6G-nZvVI-A4VU1KKfA7Xrl4JKbOK01QVOFo8aESxHMPnp56bjDUE~DAtn2KOncsGZTDg0BQbrrkNxcDC7bx01Ytq9bBLBPNYqyl5Rp3eKJj45mQvwnfb45tfWCqhavblfcWho47JoaYB-jAavgVWPyOnQ1UT6OX1Wsw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4',
		title: 'Кейт Беррі',
		userId: '5',
		content:
			'Я дуже задоволений житлом, яке я орендую у цій компанії. Жило безпечне, чисте і добре облаштоване. ',
	},
]
const reviewsPage = 5

const Reviews = () => {
	const [valuePag, setValuePag] = useState(1)
	const incPag = () => {
		if (valuePag < reviewsPage) {
			setValuePag(valuePag + 1)
		}
	}
	const decPag = () => {
		if (valuePag > 1) {
			setValuePag(valuePag - 1)
		}
	}
	const width = useScreenWidth()
	let reviewsCount = 2
	if (width === 'tablet' || width === 'desktop') {
		reviewsCount = 4
	}
	const reviewArray = reviews.slice(0, reviewsCount)

	return (
		<div className={styles.reviews}>
			<div className={styles.title}>
				<div className={styles.titleBox}>
					<h2>Відгуки</h2>
					<div className={styles.paginationBox}>
						<CircleButtonWithContent
							propClass='whiteBg'
							hoverColorBg='var(--secondary-color)'
						>
							<span className={classNames(styles.selectedPage, 'whiteText')}>
								{valuePag}
							</span>
						</CircleButtonWithContent>
						<PaginationBtn incFun={incPag} decFun={decPag} />
					</div>
				</div>
				{width === 'desktop' && (
					<CircleButtonWithContent
						propClass='whiteBg'
						hoverColorBg='var(--secondary-color)'
					>
						<DotIcon hoverColor='white' />
					</CircleButtonWithContent>
				)}
			</div>
			<div className={styles.reviewsBlock}>
				{reviewArray.map((review, index) => (
					<div className={styles.textBlockWrapper} key={review.userId}>
						<TextBlock
							key={review.userId}
							img={review.img}
							title={review.title}
							content={review.content}
							maxH={width === 'tablet' ? '10' : ''}
							height={width === 'desktop' ? '' : ''}
							fontSize={width === 'desktop' ? '1.125' : ''}
							notArrow={width === 'desktop' ? true : false}
						/>
					</div>
				))}
			</div>
		</div>
	)
}

export default Reviews

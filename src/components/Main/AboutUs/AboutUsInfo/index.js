import React from 'react'
import MainButton from './../../../MainButton/'
import styles from './AboutUsInfo.module.scss'

const AboutUsInfo = ({ width }) => {
	return (
		<>
			{(width === 'tablet' || width === 'desktop') && (
				<div
					className={styles.aboutUsInfo}
					style={{
						maxWidth: '30%',
					}}
				>
					<div>
						<div>
							<span>2013 р.</span>
							<p>Дата заснування</p>
						</div>
						<div>
							<span>12 497</span>
							<p>Задоволених клієнтів</p>
						</div>
						<div>
							<span>1 500 +</span>
							<p>Активних працівників</p>
						</div>
					</div>

					<MainButton text='Більше' propClass='whiteBtn' />
				</div>
			)}
		</>
	)
}

export default AboutUsInfo

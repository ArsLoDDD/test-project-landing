import React from 'react'
import styles from './AboutUs.module.scss'
import TextBlock from '../../TextBlock'
import ItemBox from '../../ItemBox'
import AboutUsInfo from './AboutUsInfo'
import PhotoBox from './PhotoBox'

import useScreenWidth from '../../../hooks/useScreenWidth'

const textBlockContent =
	'Ми є компанією з оренди соціального житла, яка прагне надавати доступне та якісне житло людям з низьким доходом. У нас є широкий вибір квартир у різних районах міста, і ми пропонуємо широкий спектр послуг, щоб допомогти нашим мешканцям жити комфортно та безпечно.'

const AboutUs = () => {
	const width = useScreenWidth()

	return (
		<div className={styles.aboutUs}>
			{width !== 'desktop' && (
				<TextBlock title='Про нас' content={textBlockContent} />
			)}

			<div className={styles.flexContainer}>
				<div className={styles.companyPersons}>
					<div>
						{width === 'desktop' && (
							<TextBlock
								title='Про нас'
								width='88'
								maxH='7.5'
								content={textBlockContent}
								fontSize='1'
								fullWidth='true'
							/>
						)}

						<div className={styles.aboutUsItems}>
							<ItemBox
								height='9'
								itemName='Лара Клек'
								description='Головний директор компанії “Dream”'
								imgLink='https://s3-alpha-sig.figma.com/img/473e/4f6e/db44d3ffbf11d4bad3cc7a6830822332?Expires=1693180800&Signature=KZU0Ia5EE0pVANLKsOU0PH3SjFG0u-Qdz6MY7nVo78-Idosmivi9kXv843nyjPi1OECjoivHZpunQCDraclMI17DirfDSb7GU1I8wgt~SC1jKGaZs0Djnu0t5A9~Bl-Ro4SRljL0ZE1J2~3HPzBIZz1pKHneOEjRBHdZRgAXHUEPOKt0QIhkO9gaG3YX6HvZEc5WYsIj87y81mDJmfQf2WFB7vSQzQLRQtFpvUwTd~2PYkAxO-ycAxcOKwpZ0WjtSfi9wJV11WjKgSTZXGXAQ7gYm2SrKLXUoRAubaQ~WOyTUkooOXp3qeUCBmQjr6LS0qAI0TdwapR0hnHqd1jFow__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'
							/>
							<ItemBox
								height='9'
								itemName='Kiра Клек'
								description='Головний директор компанії “Dream”'
								imgLink='https://s3-alpha-sig.figma.com/img/c1a9/9c0f/1fb590496dd3f8eeb8d71ebafd1d3965?Expires=1693180800&Signature=TcTV0HAN5zkEQJ2BmiYGserqwylG8qQXXoy5NUpSE1vJgK8K6GNUvFpayMbSX80JggBji11aZPxHm9plph3I4iNhy~R4WvsIK2kZOntWJ2RFXNOnY8xrY7ece-Dsq-s3xRopSAM8X5T8B6SaLoppFl~itmdnAtL5aXbqcYO0wRJwe5F5jW8uOiaRrNDtV0cvRlMgli-YI3-5Qid5ga5PobjPrHwBtwzfD2b3BThvXmWRTtYAThtPKsm6GxGpxrNMrV5rDYfMb3MqZ7l1IoqyOWMxQj88eAj12GyK1Js1gFC8R1hXg404Xj9J0pNcbm8NOpVN6dEauieT9xbRtXoyvw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'
							/>
							<AboutUsInfo width={width} />
						</div>
					</div>
				</div>
				{width === 'desktop' && (
					<div className={styles.PhotoBox}>
						<PhotoBox />
					</div>
				)}
			</div>
		</div>
	)
}

export default AboutUs

import React from 'react'
import styles from './PhotoBox.module.scss'
const defaultImg =
	'https://s3-alpha-sig.figma.com/img/2308/96dc/3bb08259e6bb9709135dc68cb1e4d268?Expires=1693180800&Signature=nmHiMR4aszeJfvBr7WWI-n5dsndRMRkNbE1h2d-Mn2PoKFvljLvmaficqE7f7UCSF~k8nutA6FbBZgpRLOlFhCm4nzAXYe1zosp8w5euhtAhR0pJUV3DtmmliTq6B4f20YMDHpgSLNOxZ1jLrBZsVRttiUXhprMi6buqpTA5CCQQuvUSI71vwcqcGICCpzRiygBX25OYvcL-04Rcr-6MwkkblMcFSMFDnrZaux5UvZe38BhEEXTLdccHcERBpTVxPwMsPmhayz5vBVQbu9gQgFWEOAIU8w-xKXrKzQ6wj0q~jiX1RT-7oswdH46heXnf6U0yvyhtYcuTr9oBPWia6g__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'
const PhotoBox = ({ link = defaultImg }) => {
	return (
		<div
			className={styles.root}
			style={{
				backgroundImage: `url(${link})`,
			}}
		></div>
	)
}

export default PhotoBox

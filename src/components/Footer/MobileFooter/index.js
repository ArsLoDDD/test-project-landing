import React from 'react'
import styles from './MobileFooter.module.scss'
import CircleButtonWithContent from '../../CircleButtonWithContent/'
import PersonIcon from '../../icons/PersonIcon'

const MobileFooter = () => {
	return (
		<div className={styles.root}>
			<div className={styles.contentWrap}>
				<div className={styles.footerLogoWrapper}>
					<a className={styles.footerLogo} href='/'>
						Dream
					</a>
				</div>

				<div className={styles.mainBox}>
					<div className={styles.addressBoxWrapper}>
						<div className={styles.addressBox}>
							<p className={styles.addressText}>
								766 9th Ave #2, New York, United States
							</p>
							<a href='tel:+1212581099'>+1 212 581 099</a>
						</div>
						<div className={styles.linkBox}>
							<CircleButtonWithContent
								propClass='contentBg'
								hoverColorBg='var(--primary-color)'
							>
								<PersonIcon hoverColor='var(--content-bg)' />
							</CircleButtonWithContent>
						</div>
					</div>

					<div className={styles.cityListWrapper}>
						<ul className={styles.cityList}>
							<li>
								<a href='/'> Atlanta Real Estate</a>
							</li>
							<li>
								<a href='/'> Austin Real Estate</a>
							</li>
							<li>
								<a href='/'> San Francisco</a>
							</li>
							<li>
								<a href='/'> Real Estate</a>
							</li>
							<li>
								<a href='/'> Los Angeles</a>
							</li>
							<li>
								<a href='/'> Real Estate</a>
							</li>
						</ul>

						<div className={styles.privacy}>
							<a href='/'>Privacy</a>
							<a href='/'>Terms</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default MobileFooter

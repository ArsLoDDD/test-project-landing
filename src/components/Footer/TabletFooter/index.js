import React from 'react'
import styles from './TabletFooter.module.scss'
import useScreenWidth from '../../../hooks/useScreenWidth'
import CircleButtonWithContent from '../../CircleButtonWithContent'
import PersonIcon from '../../icons/PersonIcon'
import ArrowIcon from '../../icons/ArrowIcon'

const TabletFooter = () => {
	const width = useScreenWidth()
	return (
		<div className={styles.root}>
			<div className={styles.contentWrapper}>
				<div className={styles.frstCol}>
					<div>
						<a className={styles.footerFrsLogo} href='/'>
							Dream
						</a>
						<div className={styles.frstColContent}>
							<p className={styles.addressText}>
								766 9th Ave #2, New York, United States
							</p>
							<a href='tel:+1212581099'>+1 212 581 099</a>
						</div>
					</div>
					<span>© Dream 2023</span>
				</div>
				<div className={styles.scdCol}>
					<div>
						<div>
							<a className={styles.footerScdLogo} href='/'>
								Popular real estate markets
							</a>
						</div>
						<div className={styles.scdColContent}>
							<ul className={styles.cityList}>
								<li>
									<a href='/'> Atlanta Real Estate</a>
								</li>
								<li>
									<a href='/'> Austin Real Estate</a>
								</li>
								<li>
									<a href='/'> San Francisco Real Estate</a>
								</li>
								<li>
									<a href='/'> Los Angeles Real Estate</a>
								</li>
							</ul>
						</div>
					</div>
					<div className={styles.privacy}>
						<a href='/'>Privacy</a>
						<a href='/'>Terms</a>
					</div>
				</div>
				{width === 'desktop' && (
					<>
						<div className={styles.thrdCol}>
							<div>
								<div>
									<a className={styles.footerThrdLogo} href='/'>
										Search by popular states
									</a>
								</div>
								<div className={styles.thrdColContent}>
									<ul className={styles.scdLityList}>
										<li>
											<a href='/'> California Real Estate</a>
										</li>
										<li>
											<a href='/'> New York Estate</a>
										</li>
										<li>
											<a href='/'> Ohio Real Estate</a>
										</li>
										<li>
											<a href='/'> New Jersey Real Estate </a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div className={styles.footerIconBox}>
							<CircleButtonWithContent
								propClass='whiteBg'
								linkUrl='/'
								hoverColorBg='var(--dark-primary-color)'
							>
								<PersonIcon hoverColor='white' />
							</CircleButtonWithContent>
							<CircleButtonWithContent
								propClass='whiteBg'
								linkUrl='/'
								hoverColorBg='var(--dark-primary-color)'
								hoverRotate='true'
							>
								<ArrowIcon rotate='90' hoverColor='white' />
							</CircleButtonWithContent>
						</div>
					</>
				)}
			</div>
		</div>
	)
}

export default TabletFooter

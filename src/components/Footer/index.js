import React from 'react'
import useScreenWidth from '../../hooks/useScreenWidth'
import MobileFooter from './MobileFooter/'
import TabletFooter from './TabletFooter/'


const Footer = () => {
	const width = useScreenWidth()
	return (
		<>
			{width === 'mobile' && <MobileFooter />}
			{(width === 'tablet' || width === 'desktop') && <TabletFooter />}
		</>
	)
}

export default Footer

import { useState, useEffect } from 'react';
import debounce from 'lodash/debounce';

const useScreenWidth = () => {
  const [screenWidth, setScreenWidth] = useState('');

  useEffect(() => {
    const handleResize = () => {
      const width = window.innerWidth;

      if (width < 768) {
        setScreenWidth('mobile');
      } else if (width >= 768 && width < 1024) {
        setScreenWidth('tablet');
      } else {
        setScreenWidth('desktop');
      }
    };

    const debouncedHandleResize = debounce(handleResize, 200);

    window.addEventListener('resize', debouncedHandleResize);

    handleResize();

    return () => {
      window.removeEventListener('resize', debouncedHandleResize);
    };
  }, []);

  return screenWidth;
};

export default useScreenWidth;
